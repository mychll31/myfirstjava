package com.example.myfirstjava;

import java.util.Scanner;

/**
 * Created by admin on 4/30/16.
 * Sample Project
 */
public class MyfirstJava {

    public static void main (String[] args) {
        System.out.print("**********************\n   MY FIRST JAVA APP\n**********************\n\n");

        Scanner scan = new Scanner(System.in);
        System.out.print("Name: ");
        String name = scan.nextLine();

        System.out.println("Hi " + name + "!\nChoose Menu:[0]-> Exit [1]-> Add [2]-> Edit [3]-> Delete [4]-> Search");

    }

}
